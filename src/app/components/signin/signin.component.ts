import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/authorization/AuthService';
import { Router } from '@angular/router';
import { LoginRequest } from 'src/app/model/LoginRequest';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
signinForm: FormGroup;
nullValueErrorMessage : boolean = false;
invalid : boolean = false;
  constructor(
    public fb: FormBuilder,
    public authService: AuthService,
    public router: Router)
    //change 2 
    {
      this.signinForm = this.fb.group({
      "username": new FormControl ("",[Validators.required] ),
      "password": new FormControl("",[Validators.required]),
    });
  }
  //password-empty
  passwordEmpty(){
    if(this.signinForm.controls['password'].value.length!=0){
      this.nullValueErrorMessage = false;
    }
    else{
      this.nullValueErrorMessage = true;
    }
  }
  //username empty
  //password-empty
  usernameEmpty(){
    if(this.signinForm.controls['username'].value.length!=0){
      this.nullValueErrorMessage = false;
    }
    else{
      this.nullValueErrorMessage = true;
    }
  }
  ngOnInit(): void {
    //change 3
  }
  loginUser() {
    if(this.signinForm.controls['username'].value.length!=0 && this.signinForm.controls['password'].value.length!=0){
      let loginCredentials = new LoginRequest(this.signinForm.controls['username'].value,this.signinForm.controls['password'].value);
      this.authService.signIn(loginCredentials);
       this.router.navigate(['/user-dashboard']);
       this.nullValueErrorMessage = false;
    }
    else{
      this.nullValueErrorMessage = true;
    }
    // this.authService.signIn(this.signinForm.value);
    // this.router.navigate(['/user-dashboard']);
  }
 
  
}

 