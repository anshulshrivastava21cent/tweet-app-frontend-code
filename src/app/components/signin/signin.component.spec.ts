import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { AuthService } from 'src/app/authorization/AuthService';
import { User } from 'src/app/model/User';

import { SigninComponent } from './signin.component';
const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
const loginServiceSpy = jasmine.createSpyObj('LoginService', ['login']);
describe('SigninComponent', () => {
  let component: SigninComponent;
  let fixture: ComponentFixture<SigninComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SigninComponent ]
    })
    .compileComponents();
    providers: [
      { provide: AuthService, useValue: AuthService }
    ]

    fixture = TestBed.createComponent(SigninComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should call the login method when the component does something', 
  inject([AuthService], ((userService: AuthService) => {
   const userObject = {id:"62f901afd52ea944da407eb7",username:"admin",
   email:"admin@gmail.com",roles:(['user']),password:"123456789",firstName:"admin",
   lastName:"a",contactNumber:"6384807832"}
    spyOn(userService, 'login');
    let component = fixture.componentInstance;
    component.authService.signIn(userObject);
    expect(userService.signIn).toHaveBeenCalled();})));
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
});
