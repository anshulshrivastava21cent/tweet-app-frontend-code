import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { AuthService } from 'src/app/authorization/AuthService';

import { SignupComponent } from './signup.component';

describe('SignupComponent', () => {
  let component: SignupComponent;
  let fixture: ComponentFixture<SignupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignupComponent ]
    })
    .compileComponents();
    providers: [
      { provide: AuthService, useValue: AuthService }
    ]

    fixture = TestBed.createComponent(SignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should call the login method when the component does something', 
  inject([AuthService], ((userService: AuthService) => {
   const userObject = {id:"62f901afd52ea944da407eb7",username:"admin",
   email:"admin@gmail.com",roles:(['user']),password:"admin@123",firstName:"admin",
   lastName:"a",contactNumber:"6384807832"}
    spyOn(userService, 'signup');
    let component = fixture.componentInstance;
  //  component.authService.signUp(userObject);
    expect(userService.signUp).toHaveBeenCalled();})));
  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
