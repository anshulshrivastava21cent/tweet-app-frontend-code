import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/authorization/AuthService';
import { Router } from '@angular/router';
import { SignUpRequest } from 'src/app/model/SignUpRequest';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
   signupForm: FormGroup;
   message : String = "";
   //validation variables
   passwordValidation: boolean = false;
  prePasswordCheck: boolean = false;
  passwordLength: boolean = false;
  emailValid: boolean = false;
  //contactValid : boolean = false;
  phoneValidation: boolean = false;
  phoneValidationIsNan: boolean = false;
  sussesfullyRegistered: boolean = false;
  showElement: boolean = false;
  
  emailNotAvailable: boolean = false;

  usersList: Array<SignUpRequest>=[];
  email: String ="";
  constructor(
    public fb: FormBuilder,
    public authService: AuthService,
    public router: Router
  ) {
    
    this.signupForm = this.fb.group({
      "username": new FormControl("",[Validators.required]),
      "firstName":new FormControl("",[Validators.required]),
      "lastName":new FormControl("",[Validators.required]),
      "email": new FormControl("",[Validators.required]),
      "contactNumber": new FormControl("",[Validators.required]),
      "password": new FormControl("",[Validators.required]),
    });
    
  }

  ngOnInit(): void {
  }
//validations
//check if password and repassword are same

checkPassword(password: HTMLInputElement, rePassword: HTMLInputElement) {
  if (password.value.length != 0) {
    this.prePasswordCheck = false;
    if (password.value == rePassword.value || rePassword.value.length == 0) {
      this.passwordValidation = false;
    } else {
      this.passwordValidation = true;
    }
  } else {
    this.prePasswordCheck = true;
  }
  if (rePassword.value.length == 0) {
    this.prePasswordCheck = false;
  }
  if (this.prePasswordCheck) {
    rePassword.value = "";
  }
}

// Check prePassword
checkPrePassword(password: HTMLInputElement) {
  if (password.value.length >= 6 && password.value.length <= 12) {
    this.passwordLength = false;
  }
  else {
    this.passwordLength = true;
  }
  if (password.value.length != 0) {
    this.prePasswordCheck = false;
  } else {
    this.passwordLength = false;
  }
}
 //Email Validation

 emailValidation(email: HTMLInputElement) {
  if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email.value)) {
    this.emailValid = false;
  }
  else {
    this.emailValid = true;
  }
  if (email.value.length == 0) {
    this.emailValid = false
  }
}

// validate Phone number
phoneNumberValidaton(number: HTMLInputElement) {
  let mobilePattern = "^((\\+91-?)|0)?[0-9]{10}$";
  if (number.value.match(mobilePattern) || number.value.length == 0) {
    this.phoneValidation = false;
  } else {
    this.phoneValidation = true;
  }
  if (!Number.isNaN(parseInt(number.value)) || number.value.length == 0) {
    this.phoneValidationIsNan = false;
  }
  else {
    this.phoneValidationIsNan = true;
  }
}



  registerUser() {
    if(!this.emailValid && !this.passwordLength &&!this.passwordValidation &&
      !this.phoneValidation) {
        if (this.signupForm.controls['username'].value.length != 0 &&
        this.signupForm.controls['email'].value.length != 0 && 
        this.signupForm.controls['password'].value.length != 0 &&
        this.signupForm.controls['firstName'].value.length != 0 &&
        this.signupForm.controls['lastName'].value.length != 0 &&
        this.signupForm.controls['contactNumber'].value.length != 0) {

        let user = new SignUpRequest(
          this.signupForm.controls['username'].value,
          this.signupForm.controls['email'].value,
          this.signupForm.controls['password'].value,
          this.signupForm.controls['firstName'].value,
          this.signupForm.controls['lastName'].value,
          this.signupForm.controls['contactNumber'].value)

          this.authService.signUp(this.signupForm.value).subscribe((_response) => {
            // console.log(response);
            // console.log("register user");
            this.signupForm.controls['firstName'].reset();
            this.signupForm.controls['lastName'].reset();
            this.signupForm.controls['email'].reset();
            this.signupForm.controls['password'].reset();
            this.signupForm.controls['username'].reset();
            this.signupForm.controls['contact'].reset();
            this.showElement = true;
            this.router.navigate(['login']);
          },
            // failure function
            _failureData => {
              this.emailNotAvailable = true;
              
            });
        }
    }
    this.authService.signUp(this.signupForm.value).subscribe((res) => {
      // if (res.result) {
      //   this.signupForm.reset();
      //   this.router.navigate(['login']);
      // }
      this.router.navigate(['login']);
      window.alert("registered successfully, now you will redirect to login page");
      this.message = res.message;
      console.log(res);
    });
  }

   
}
