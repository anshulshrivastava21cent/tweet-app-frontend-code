import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/authorization/AuthService';
import { Router } from '@angular/router';
import { TweetRequest } from 'src/app/model/TweetRequest';
import { TweetserviceService } from 'src/app/services/tweetservice.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Tweet } from 'src/app/model/Tweet';
import { MessageResponse } from 'src/app/model/MessageResponse';
@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.scss']
})
export class UserDashboardComponent implements OnInit {
 
  ngOnInit() {
    if (!this.authService.getisLoggedIn()) {
      this.router.navigate(['login']);
    }
    this.getAllTweet();
  }
  replyRes: boolean=false;
  editRes: boolean = false;
  replyenable: boolean = false;
  editenable: boolean = false;
  likeRes: boolean = false;
  deleteRes: boolean = false;
  searchTweetEnable: boolean = false;
  searchTweetRestlt: boolean = false;
  currentUser: Object = {};
  public show: boolean = false;
  public buttonName: any = 'Show';
  myFormGroup: any;
  deleteId: string = "";
  editMessage: string= "";
  replyMessage: string= "";
  //post-tweet
  tweetMessage = '';
  username: any = '';
  searchname: string = '';
  tag = '';
  searchEnable: boolean = false;
  searchRestlt: boolean = false;
  posttweet: boolean = false;
  tweetRequest: TweetRequest = new TweetRequest(this.tweetMessage, this.username, this.tag);
  submitted = false;
  allTweet: any = [];
  searchUserRes: any;
  emailSearch: string = "";
  userNameSearch: string = "";
  firstNameSearch: string = "";
  lastNameSearch: string = "";
  contactNumderSearch: string = "";
  constructor(
    public authService: AuthService,
    private actRoute: ActivatedRoute,
    public router: Router,
    public tweetService: TweetserviceService,
    public formBuilder: FormBuilder
  ) {
    let id = this.actRoute.snapshot.paramMap.get('id');
    this.authService.getUserProfile(id).subscribe((res) => {
      this.currentUser = res.msg;
    });
  }
  //post-tweet
  postTweet(): void {
    this.posttweet = true;
  }
  newPost(): void {
    this.submitted = false;
    this.tweetRequest = new TweetRequest(this.tweetMessage, this.username, this.tag);
  }
  closeResponse() {
    this.replyenable=false;
    this.submitted = false;
    this.deleteRes = false;
    this.likeRes = false;
    this.editRes=false;
    this.replyRes=false;
    this.getAllTweet();
  }
  closeSearch() {
    this.searchRestlt = false;
    this.searchname = "";
    this.emailSearch = "";
    this.userNameSearch = "";
    this.firstNameSearch = "";
    this.lastNameSearch = "";
    this.contactNumderSearch = "";
  }
 
  save(): void {
    this.submitted = true;
    this.posttweet = false;
    this.username = sessionStorage.getItem('username') ? sessionStorage.getItem('username') : "";
    this.tweetService.postTweet(this.tweetRequest)
      .subscribe(data => console.log(data), error => console.log(error));
    this.tweetRequest = new TweetRequest(this.tweetMessage, this.username, this.tag);
  }

  //get all tweet

  getAllTweet() {
    //this.posttweet=false;
    this.tweetService.getAllTweets()
      .subscribe(data => this.allTweet = data, error => console.log(error));
    console.log(this.allTweet);
  }

  //search user

  searchDialog() {
    this.searchEnable = true;
  }
  searchTweetDialog() {
    this.searchTweetEnable = true;
  }
  searchUser() {
    this.searchEnable = false;
    this.searchRestlt = true;
    console.log(this.searchname);
    // this.searchUserRes =this.tweetService.getUser(this.searchname);
    this.tweetService.getUser(this.searchname)
      .subscribe(data => this.setuserdetails(data), error => console.log(error));
  }
  searchTweet() {
    this.searchTweetEnable = false;
    this.searchTweetRestlt = true;
    console.log(this.searchname);
    // this.searchUserRes =this.tweetService.getUser(this.searchname);
    this.tweetService.getUserTweet(this.searchname)
      .subscribe(data => this.allTweet = data, error => console.log(error));
  }
  setuserdetails(searchUserRes: any) {
    console.log(searchUserRes);
    this.emailSearch = searchUserRes.email;
    this.userNameSearch = searchUserRes.username;
    this.firstNameSearch = searchUserRes.firstName;
    this.lastNameSearch = searchUserRes.lastName;
    this.contactNumderSearch = searchUserRes.contactNumber;
  }


}
