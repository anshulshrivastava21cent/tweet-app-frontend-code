import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from  '@angular/common/http/testing';

import { UserDashboardComponent } from './user-dashboard.component';
import { TweetserviceService } from 'src/app/services/tweetservice.service';

describe('UserDashboardComponent', () => {
  let component: UserDashboardComponent;
  let fixture: ComponentFixture<UserDashboardComponent>;
  let httpTestingController: HttpTestingController;
   let tweetService : TweetserviceService;

const mockData = {
  "status": "success", 
  "data": [{
    "tweetMessage": "Testing-Tweet", 
    "username": "test-user", 
    "tag": "#test"
  },
  {
    "username":"testuser",
    "id":"62f901afd52ea944da407eb7",
    "tweetMessage":"Testing-Tweet"
  }
 ]
  };
  beforeEach(async () => {
     await TestBed.configureTestingModule({
      providers: [ 
       TweetserviceService, 
       { provide: 'url', 
         useValue: 'apiurl'
       }],
      imports: [HttpClientTestingModule]
    });
    tweetService = TestBed.inject(TweetserviceService);
    httpTestingController = TestBed.get(HttpTestingController);
      declarations: [ UserDashboardComponent ]
    })
    afterEach(() => { 
      httpTestingController.verify(); 
     }); 
     //post-tweet
     it('create should make a POST HTTP request with resource as body', () => {
      const createObj = { tweetMessage: "testtweet", username:"testuser",tag:"#test" };
      tweetService.postTweet(createObj).subscribe(res=>{
        expect(res.tweetObject).toBe('testtweet')
      });
      const req = httpTestingController.expectOne('apiUrl', 'post to api');
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toBe(createObj);
      expect(req.cancelled).toBeFalsy(); 
      expect(req.request.responseType).toEqual('json');
      req.flush(createObj);
      httpTestingController.verify();
      });
      //update
      it('Update should make a PUT HTTP request with resource as body', () => {
        const updateObj = { tweetMessage: "testtweet", username:"testuser",tag:"#test" };
        tweetService.editTweet("test-user","62f901afd52ea944da407eb7","test-tweet").subscribe(res=>{
          expect(res).toBe('test-tweet')
        });
        const req = httpTestingController.expectOne('apiUrl', 'post to api');
        expect(req.request.method).toBe('PUT');
        expect(req.request.body).toBe(updateObj);
        expect(req.cancelled).toBeFalsy(); 
        expect(req.request.responseType).toEqual('json');
        req.flush(updateObj);
        httpTestingController.verify();
        });
        //delete
      it('delete should make a delete HTTP request with resource as body', () => {
          const deleteObj = { username: "testuser", id:"62f901afd52ea944da407eb7" };
          tweetService.deleteTweet("test-user","62f901afd52ea944da407eb7").subscribe(res=>{
            expect(res).toBe('62f901afd52ea944da407eb7')
          });
          const req = httpTestingController.expectOne('apiUrl', 'post to api');
          expect(req.request.method).toBe('DELETE');
          expect(req.request.body).toBe(deleteObj);
          expect(req.cancelled).toBeFalsy(); 
          expect(req.request.responseType).toEqual('json');
          req.flush(deleteObj);
          httpTestingController.verify();
          });
      //getalltweets
      it('getAllTweets should make a GET HTTP request and return all data items', () => {
        tweetService.getUserTweet("test-user").subscribe(res => {
          expect(res).toBe(1); 
         }); 
        const req = httpTestingController.expectOne('apiUrl');
        expect(req.request.method).toBe('GET');
        expect(req.cancelled).toBeFalsy(); 
        expect(req.request.responseType).toEqual('json');
        req.flush(mockData);
        httpTestingController.verify();
       });
       //getallusers
      it('getAllUsers should make a GET HTTP request and return all data items', () => {
        tweetService.getUser("test-user").subscribe(res => {
          expect(res).toBe(1); 
         }); 
        const req = httpTestingController.expectOne('apiUrl');
        expect(req.request.method).toBe('GET');
        expect(req.cancelled).toBeFalsy(); 
        expect(req.request.responseType).toEqual('json');
        req.flush(mockData);
        httpTestingController.verify();
       });
       //getusertweet
       it('getusertweet should make a GET HTTP request and return all data items', () => {
        tweetService.getUserTweet("test-user").subscribe(res => {
          expect(res).toBe(1); 
         }); 
        const req = httpTestingController.expectOne('apiUrl');
        expect(req.request.method).toBe('GET');
        expect(req.cancelled).toBeFalsy(); 
        expect(req.request.responseType).toEqual('json');
        req.flush(mockData);
        httpTestingController.verify();
       });
       //liketweet
       it('Like should make a PUT HTTP request and return all data items', () => {
        tweetService.likeTweet("test-user","62f901afd52ea944da407eb7").subscribe(res => {
          expect(res).toBe(1); 
         }); 
        const req = httpTestingController.expectOne('apiUrl');
        expect(req.request.method).toBe('PUT');
        expect(req.cancelled).toBeFalsy(); 
        expect(req.request.responseType).toEqual('json');
        req.flush(mockData);
        httpTestingController.verify();
       });
       //ReplyTweet
       it('Reply should make a {POST HTTP request and return all data items', () => {
        tweetService.replyTweet("test-user","62f901afd52ea944da407eb7","twest-tweet").subscribe(res => {
          expect(res).toBe(1); 
         }); 
        const req = httpTestingController.expectOne('apiUrl');
        expect(req.request.method).toBe('POST');
        expect(req.cancelled).toBeFalsy(); 
        expect(req.request.responseType).toEqual('json');
        req.flush(mockData);
        httpTestingController.verify();
       });
     });


   
  it('should create', () => {
    expect(component).toBeTruthy();
  });

function component(component: any) {
  throw new Error('Function not implemented.');
}

