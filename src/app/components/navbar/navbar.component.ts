import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserDashboardComponent } from '../user-dashboard/user-dashboard.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {
  loginStatus$ = new BehaviorSubject<boolean>(true);
  username$ = Observable<String>;
  constructor(private userDashboard:UserDashboardComponent, private router:Router) { }

  ngOnInit(): void {
  }

}
