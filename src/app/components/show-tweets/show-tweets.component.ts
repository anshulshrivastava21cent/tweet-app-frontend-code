import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/authorization/AuthService';
import { Tweet } from 'src/app/model/Tweet';
import { TweetserviceService } from 'src/app/services/tweetservice.service';

@Component({
  selector: 'app-show-tweets',
  templateUrl: './show-tweets.component.html',
  styleUrls: ['./show-tweets.component.sass' ,'../user-dashboard/user-dashboard.component.scss']
})

export class ShowTweetsComponent implements OnInit {
 @Input() tweet:Tweet = {
   id: "",
   username: "",
   tweetMessage: "",
   timeStamp: 0,
   tag: "",
   replies: [],
   likes: 0,
   isReplies: false
 };
 replyRes: boolean=false;
 editRes: boolean = false;
 replyenable: boolean = false;
 editenable: boolean = false;
 likeRes: boolean = false;
 deleteRes: boolean = false;
 searchTweetEnable: boolean = false;
 searchTweetRestlt: boolean = false;
 currentUser: Object = {};
 public show: boolean = false;
 public buttonName: any = 'Show';
 myFormGroup: any;
 deleteId: string = "";
 editMessage: string= "";
 replyMessage: string= "";
 //post-tweet
 tweetMessage = '';
 username: any = '';
 searchname: string = '';
 tag = '';
 searchEnable: boolean = false;
 searchRestlt: boolean = false;
 posttweet: boolean = false;
 submitted = false;
 allTweet: any = [];
 searchUserRes: any;
 emailSearch: string = "";
 userNameSearch: string = "";
 firstNameSearch: string = "";
 lastNameSearch: string = "";
 contactNumderSearch: string = "";
  userAllTweets : Tweet[]= [];
  constructor(public tweetService:TweetserviceService, public router:Router, public authService: AuthService ) { }
  showTweets(){
    this.tweetService.getAllTweets().subscribe((response : any) =>{
        this.userAllTweets = response;
        console.log("userAllTweets", this.userAllTweets);
    })
  }
  

  ngOnInit(): void {
    this.showTweets();
  }

  closeResponse() {
    this.replyenable=false;
    this.deleteRes = false;
    this.likeRes = false;
    this.editRes=false;
    this.replyRes=false;
  }

 enableEdit(){
    this.editenable=true;
  }
  edit(userName: string,id:string){

    this.editenable=false;
    this.editRes=true;
    this.tweetService.editTweet(userName,id,this.editMessage)
      .subscribe(data => console.log(data), error => console.log(error));    
  }

  enableReply(){
    this.replyMessage="";
    this.replyenable=true;
  }
  replyTweet(userName:string,id:string){
    this.replyenable=false;
    this.replyRes=true;
    this.tweetService.replyTweet(userName,id,this.replyMessage)
    .subscribe(data => console.log(data), error => console.log(error));
  }
   //delete Tweet

   deleteTweet(userName: string, id: string) {
    console.log(this.allTweet);
    console.log(id);
    this.tweetService.deleteTweet(userName, id)
      .subscribe(data => console.log(data), error => console.log(error));
    this.deleteRes = true;
  }

  //like tweet
  likeTweet(userName: string, id: string) {
    console.log(id);
    this.tweetService.likeTweet(userName, id)
      .subscribe(data => console.log(data), error => console.log(error));
    this.likeRes = true;
  }
}
