import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/authorization/AuthService';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {
  forgetForm!: FormGroup;
  currentUser: any =[];
  username : string = '';
  newPassword : string = '';
  public fb!: FormBuilder;
  result: boolean =false;

  constructor(public router : Router , public authService : AuthService) {
   // this.username = sessionStorage.getItem('username') ? sessionStorage.getItem('username') : "";
   }
//Forget-Password
forgot(){
 // this.username = sessionStorage.getItem('username') ? sessionStorage.getItem('username') : "";
 console.log(this.username);
 this.result=true;
 console.log(this.newPassword);
  this.authService.forgetPassword(this.username, this.newPassword).subscribe((res: { msg: any; }) => {
    this.currentUser = res.msg;
    console.log(this.currentUser)
   
  });
  
  this.router.navigate(['/login']);
}


  ngOnInit(): void {
  }
  closeResponse() {
    this.result=false;
  }
  
}

