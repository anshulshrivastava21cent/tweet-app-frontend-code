export class JwtResponse{
   
    token: String;
    id : String;
    username: String;
    email : String;
    roles : String[];
    constructor( token: String, id : String, username: String, email : String, roles : String[]){
        this.token = token;
        this.id = id;
        this.username = username;
        this.email = email;
        this.roles = roles;
    }
}