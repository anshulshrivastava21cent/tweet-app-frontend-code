export class SignUpRequest{
      username : String;
      email : String;
      roles : Set<String> | undefined;
      password: String;
      firstName : String;
      lastName: String;
      contactNumber: String;
      constructor(username: String,email: String,password: String,firstName: String,lastName: String,contactNumber: String){
          this.username = username;
          this.email = email;
          this.password = password;
          this.firstName = firstName;
          this.lastName = lastName;
          this.contactNumber = contactNumber;
      }
}