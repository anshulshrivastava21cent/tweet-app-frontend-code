import { Roles } from "./Roles";

export class User{
    id: String;
    username: String;
    email: String;
    roles: Set<Roles>;
    password: String;
    firstName : String;
    lastName: String;
    contactNumber: String;
    constructor(id: String,username: String,email: String,roles: Set<Roles>,password: String,firstName: String,lastName: String,contactNumber: String){
        this.id = id;
        this.username = username;
        this.email = email;
        this.roles = roles;
        this. password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactNumber = contactNumber;
    }

}