import { ERole } from "./Erole";
export class Roles{
    id : String;
    name : ERole;

    constructor(id:String, name:ERole){
        this.id = id;
        this.name = name;
    }
}