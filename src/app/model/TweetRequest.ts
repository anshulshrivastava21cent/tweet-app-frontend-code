export class TweetRequest{
    tweetMessage : String;
    username : String;
    tag : String;
    constructor(tweetMessage : String,
        username : String,
        tag : String){
            this.tweetMessage = tweetMessage;
            this.username = username;
            this.tag = tag;
        }
        
}