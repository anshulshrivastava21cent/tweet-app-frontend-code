
export class Tweet{
    id : string;
    username : string;
    tweetMessage : string;
    timeStamp : number;
    tag : string;
    replies : Array<Tweet>;
    likes : number;
    isReplies : boolean;
    constructor(id: string, username:string, tweetMessage:string, timeStamp:number, tag:string, replies:Array<Tweet>,
        likes:number,isReplies:boolean){
            this.id = id;
            this.username = username;
            this.tweetMessage = tweetMessage;
            this.timeStamp = timeStamp;
            this.tag = tag;
            this.replies = replies;
            this.likes = likes;
            this.isReplies = isReplies;
        }

}