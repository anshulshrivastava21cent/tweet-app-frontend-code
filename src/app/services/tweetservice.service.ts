import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../authorization/AuthService';
import { Tweet } from '../model/Tweet';
import { TweetRequest } from '../model/TweetRequest';

@Injectable({
  providedIn: 'root'
})

export class TweetserviceService {
  endpoint: string = 'http://localhost:8081/api/v1.0/tweets/tweet';
  tweetArray : Array<Tweet> = [];
  id : string ='';
  username: string ='';
   tweetMessage: string ='';
   timeStamp!: number;
    tag: string=''; 
    replies: Tweet[] =[]; 
    likes!: number ; 
    isReplies: boolean = false
    tweetObject:  Tweet = new Tweet(this.id,this.username,this.tweetMessage,this.timeStamp,this.tag,this.replies,this.likes,this.isReplies);
  constructor(public http: HttpClient, public authService : AuthService) { }

  //1.posttweet
  postTweet(tweetRequest : TweetRequest):Observable<any>{
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Bearer "+ this.authService.getToken(), 
      'Access-Control-Allow-Origin':"http://localhost:4200"
    });
     let options = { headers: headers };
    let api = `${this.endpoint}/post`;
   // return this.http.post(api, tweetRequest).pipe(catchError(this.handleError));
    let returnPost =  this.http.post(api,tweetRequest,options);
    console.log(returnPost);
    return returnPost;
  }
  //getAllUserTweets(/all)
  getAllTweets(){
    // let headers = new HttpHeaders({'loggedInUser': sessionStorage.getItem('user')});
    // return this.http.get<TweetResponse[]>(API_URL+"/tweets/"+userName,{headers});
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Bearer "+ this.authService.getToken(), 
      'Access-Control-Allow-Origin':"http://localhost:4200"
    });
     let options = { headers: headers };
    let api = `${this.endpoint}/all`;
   // return this.http.post(api, tweetRequest).pipe(catchError(this.handleError));
    let returnallUserTweets =  this.http.get(api,options);
    console.log(returnallUserTweets);
    return returnallUserTweets;
  }

  getUserTweet(userName : string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Bearer "+ this.authService.getToken(), 
      'Access-Control-Allow-Origin':"http://localhost:4200"
    });
     let options = { headers: headers };
    let api = `${this.endpoint}/`+userName;
    let returnUserTweets =  this.http.get(api,options);
    console.log(returnUserTweets);
    return returnUserTweets;
  }

  likeTweet(userName: string, id: string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Bearer "+ this.authService.getToken(), 
      'Access-Control-Allow-Origin':"http://localhost:4200"
    });
     let options = { headers: headers };
     let api = `${this.endpoint}/`+userName+`/like/`+id;
    let returnUserTweets =  this.http.put(api,options);
    console.log(returnUserTweets);
    return returnUserTweets;
  }

  getUser(userName : String){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Bearer "+ this.authService.getToken(), 
      'Access-Control-Allow-Origin':"http://localhost:4200"
    });
     let options = { headers: headers };
    let api = `${this.endpoint}/user/search/`+userName;
    console.log(api);
    let returnUser=  this.http.get(api,options);
    console.log(returnUser);
    return returnUser;
  }

  deleteTweet(userName:string,id:string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Bearer "+ this.authService.getToken(), 
      'Access-Control-Allow-Origin':"http://localhost:4200"
    });
     let options = { headers: headers };
    let api = `${this.endpoint}/`+userName+`/delete/`+id;
    console.log(api);
    let returnUser=  this.http.delete(api,options);
    console.log(returnUser);
    return returnUser;
  }

  editTweet(userName:string,id:string,tweetMessage:string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Bearer "+ this.authService.getToken(), 
      'Access-Control-Allow-Origin':"http://localhost:4200"
    });
     let options = { headers: headers };
    let api = `${this.endpoint}/`+userName+`/update/`+id;
    console.log(api);
    let returnUser=  this.http.put(api,tweetMessage,options);
    console.log(returnUser);
    return returnUser;
  }

  replyTweet(userName:string,id:string,tweetMessage:string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Bearer "+ this.authService.getToken(), 
      'Access-Control-Allow-Origin':"http://localhost:4200"
    });
     let options = { headers: headers };
    let api = `${this.endpoint}/`+userName+`/reply/`+id;
    console.log(api);
    let returnUser=  this.http.post(api,tweetMessage,options);
    console.log(returnUser);
    return returnUser;
  }

  ngOnInit(): void {
  }

}
