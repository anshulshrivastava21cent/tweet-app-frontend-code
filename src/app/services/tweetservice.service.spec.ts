import { TestBed } from '@angular/core/testing';

import { TweetserviceService } from './tweetservice.service';

describe('TweetserviceService', () => {
  let service: TweetserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TweetserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
