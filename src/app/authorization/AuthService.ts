import { Injectable } from '@angular/core';
import { User } from '../model/User';
import { SignUpRequest } from '../model/SignUpRequest';
import {  Observable, Subject, throwError } from 'rxjs';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

import { catchError, map, subscribeOn } from 'rxjs/operators';
import{
    HttpClient,
    HttpHeaders,
    HttpErrorResponse
} from '@angular/common/http';
import { Router} from '@angular/router';
import { LoginRequest } from '../model/LoginRequest';

@Injectable({
    providedIn: 'root',
})
export class AuthService{
  
  [x: string]: any;
   isLoggedIn = false;
    endpoint: string = 'http://localhost:8088/api/v1.0/tweets/auth';
    headers = new HttpHeaders().set('Content-Type', 'application/json');
    
  currentUserObject ={};
  
    
    constructor(private http: HttpClient, public router: Router) {

    }
    // Sign-up
    signUp(signUpRequest: SignUpRequest): Observable<any> {
      let api = `${this.endpoint}/register`;
      return this.http.post(api, signUpRequest).pipe(catchError(this.handleError));
    }
    // Sign-in
    
    signIn(user: LoginRequest) {
       
      return this.http
        .post<any>(`${this.endpoint}/login`, user)
        .subscribe((res: any) => {
          sessionStorage.setItem('access_token', res.accessToken);
        });  
    }
    forgetPassword(username:string,newPassword:string):any{
      console.log(username);
      return this.http.put(this.endpoint+"/"+username+"/forget",newPassword).subscribe((res: any) => {
            sessionStorage.setItem('access_token', res.accessToken);
           });  
    }
    
    getToken() {
      return sessionStorage.getItem('access_token');
    }
    getisLoggedIn(): boolean {
      let authToken = sessionStorage.getItem('access_token');
      return authToken !== null ? true : false;
    }
    doLogout() {
      let removeToken = sessionStorage.removeItem('access_token');
      if (removeToken == null) {
        this.router.navigate(['login']);
      }
    }
    // User profile
    getUserProfile(id: any): Observable<any> {
      //changing here
      //this.object.next(true);
      let api = `${this.endpoint}/user-dashboard`;
      return this.http.get(api, { headers: this.headers }).pipe(
        map((res) => {
          return res || {};
        }),
        catchError(this.handleError)
      );
    }
    
    // Error
    handleError(error: HttpErrorResponse) {
      let msg = '';
      if (error.error instanceof ErrorEvent) {
        // client-side error
        msg = error.error.message;
      } else {
        // server-side error
        msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }
      return throwError(() => msg);
    }
}