import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/authorization/AuthService';
@Injectable({
    providedIn: 'root'
  })
  export class AuthGuard implements CanActivate {
    constructor(
      public authService: AuthService,
      public router: Router
    ) { }
    canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      // if (this.authService.isLoggedIn !== true) {
      //   window.alert("Access not allowed!");
      //   this.router.navigate(['login'])
      // }
      // if (this.authService.isLoggedIn == true) {
      //   //window.alert("Access not allowed!");
      //   this.router.navigate(['user-dashboard'])
      // }
      
      return this.authService.getisLoggedIn();
    }
}