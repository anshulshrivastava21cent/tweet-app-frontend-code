import { Component } from '@angular/core';
import { AuthService } from 'src/app/authorization/AuthService';
import { Router } from '@angular/router';
@Component({
  template: 'The href is: {{href}}',
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
//   showLogin = false;
//   showRegister = false;
// ifLoggedIn = false;
  title = 'Tweet-App';
  public href: string = "";
  
  constructor(public authService: AuthService,public router: Router) { }
  logout() {
    this.authService.doLogout()
  }
  isLoggedIn(){
    return this.authService.getisLoggedIn();
  }

 ngOninit(){
  // this.ifLoggedIn = !!this.authService.getToken();
  // if(this.ifLoggedIn){
  //   this.showLogin = false;
  //   this.showRegister = false;
  // }
  this.href = this.router.url;
  console.log(this.router.url);
  // if(this.href=='/user-dashboard'){

  // }
 }
}
